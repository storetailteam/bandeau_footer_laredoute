var settings = require("./settings.json");
settings.image = settings.image === "" ? "none" : "url(../../img/"+settings.image+")";
module.exports = function (source) {
    this.cacheable();
    var test = source
        .replace(/__PLACEHOLDER__/g, settings.name+"_"+settings.cid)
        .replace(/__FORMAT__/g, settings.format)
        .replace(/__REDIRECT__/g, settings.redirect)
        .replace(/__TARGET__/g, settings.target)
        .replace(/__IMAGE__/g, settings.image);
    return test;
};
