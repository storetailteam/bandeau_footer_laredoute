"use strict";

try {

  var sto = window.__sto,
    settings = require("./../../settings.json"),
    $ = window.jQuery,
    html = require("./main.html"),
    placeholder = settings.name+"_"+settings.cid,
    style = require("./main.css"),
    container = $(html),
    creaid = settings.creaid,
    format = settings.format;

  var fontRoboto = $('<link href="//fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
  $('head').first().append(fontRoboto);

  module.exports = {
    init: _init_()
  }

  function _init_() {

    sto.load(format, function(tracker) {

      // debugger;

      //REMOVERS FUNCTION
      var removers = {};
      removers[format] = function() {
        style.unuse();
        container.remove();
      };


      if (!$('body').hasClass('laredoute')){
        return removers;
      };

      // if (__sto.tracker().value("rp").indexOf('accueil/fiche_produit') > -1) {

      if (window.location.host == 'm.laredoute.fr' || window.location.host == "m-preview.laredoute.fr") {
        tracker.error({
          "tl": "AFOOmobileExclusion"
        })
        return removers;
        return false;
      }



        //VIEW FUNCTION
        var int, isElementInViewport;
        isElementInViewport = function(el) {
          if (typeof window.jQuery === "function" && el instanceof window.jQuery) {
            el = el[0];
          }
          var rect = el.getBoundingClientRect();
          return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            !(rect.top == rect.bottom || rect.left == rect.right) &&
            !(rect.height == 0 || rect.width == 0) &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
            rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
          );
        }

        try {

          style.use();

          $('.catalog-block>.container[data-type="histo"]').after(container);


          if ($('.sto-' + placeholder + '-container').length >= 1) {
            tracker.display();
          }

          //AB TASTY
          // On crée la promise
          var def = sto.utils.deferred();

          def.promise.then(function() {
            //IMP TRACKING
            tracker.display();
            $('.sto-' + placeholder + '-container').attr('data-state', 'visible');
          });
          // On mets à disposition la promise
          sto.globals.abtasty_creative = def;
          //---------------------------
          // LE code que AB tasty doit exécuter quand ils souhaitent afficher le format
          /*window.__sto.globals.abtasty_creative.resolve();*/



          //VIEW TRACKING
          int = window.setInterval(function() {
            if (isElementInViewport(container)) {
              tracker.view();
              window.clearInterval(int);
            }
          }, 200);


          //CLICKS
          $('.sto-' + placeholder + '-container').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            tracker.click();
            window.open(settings.redirect, settings.target);
          });


          // <editor-fold> HOMOTETHIE ***********************************************************
          // var homotethieFormat = function() {
          //   var widthContainer = $('#productListContainer').width();
          //   //height format
          //   $('.sto-' + placeholder + '-container').css('height', (widthContainer / 720 * 124) + 'px');
          // }

          // homotethieFormat();

          // </editor-fold> *********************************************************************



          //RESIZE
          var containerResized;
          window.addEventListener("resize", function() {

            // homotethieFormat();

          });
          window.setTimeout(function() {
            var evt = window.document.createEvent('UIEvents');
            evt.initUIEvent('resize', true, false, window, 0);
            window.dispatchEvent(evt);
          });

          //STORE FORMAT FOR FILTERS
          /*sto.globals["print_creatives"].push({
            html: $('.sto-' + placeholder + '-container'),
            parent_container_id: "#productList",
            type: settings.format,
            crea_id: settings.creaid,
            position: pos
          });*/

        } catch (e) {
          style.unuse();
          container.remove();
          tracker.error({
            "te": "onBuild-format",
            "tl": e
          });
        }
        // }).then(null, console.log.bind(console));
      // }
      return removers;
    });

  }

} catch (e) {
  console.log(e);
}
